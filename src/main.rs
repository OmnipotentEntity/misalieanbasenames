use ahash::RandomState;
use bimap;
use indexmap::set::IndexSet;
use itertools::Itertools;
use primal;
use std::char::from_digit;

fn encode_string(input: &String) -> u64 {
    assert!(input.len() < 64 / 5 + 1);
    input.chars().rev().fold(0_u64, |acc, c| {
        (c as u32 - 'A' as u32 + 1) as u64 | (acc << 5)
    })
}

fn decode_string(val: &u64) -> String {
    let mut val = *val;
    let mut result = String::from("");
    while val != 0 {
        result.push(char::from_u32((val & 0x1f) as u32 + 'A' as u32 - 1).unwrap());
        val >>= 5;
    }

    result
}

struct MisalianNameGenerator {
    sieve: primal::Sieve,
    particle_cache: Vec<(Option<Vec<(u32, bool)>>, Option<Vec<(u32, bool)>>)>,
    used_factor_cache: Vec<Option<u32>>,
    abbr_map: bimap::BiHashMap<u32, u64, RandomState, RandomState>,
}

impl MisalianNameGenerator {
    fn new(size: usize) -> MisalianNameGenerator {
        MisalianNameGenerator {
            sieve: primal::Sieve::new(size),
            particle_cache: vec![(None, None); size + 1],
            used_factor_cache: vec![None; size + 1],
            abbr_map: bimap::BiHashMap::with_capacity_and_hashers(
                size,
                RandomState::new(),
                RandomState::new(),
            ),
        }
    }

    fn get_root_base(&self, x: u32) -> Option<&'static str> {
        match x {
            0..=13 | 16 | 17 | 20 | 36 | 100 => Some(match x {
                0 => "nullary",
                1 => "unary",
                2 => "binary",
                3 => "trinary",
                4 => "quaternary",
                5 => "quinary",
                6 => "seximal",
                7 => "septimal",
                8 => "octal",
                9 => "nonary",
                10 => "gesimal",
                11 => "elevenary",
                12 => "dozenal",
                13 => "ker's dozenal",
                16 => "hex",
                17 => "suboptimal",
                20 => "vigesimal",
                36 => "niftimal",
                100 => "centesimal",
                _ => unreachable!(),
            }),
            _ => None,
        }
    }

    fn get_root_prefix(&self, x: u32) -> Option<&'static str> {
        match x {
            0..=13 | 16 | 17 | 20 | 36 | 100 | u32::MAX => Some(match x {
                u32::MAX => "sna",
                0 => "hen",
                1 => "un",
                2 => "bi",
                3 => "tri",
                4 => "tetra",
                5 => "penta",
                6 => "hexa",
                7 => "hepta",
                8 => "octo",
                9 => "enna",
                10 => "deca",
                11 => "leva",
                12 => "doza",
                13 => "baker",
                16 => "tesser",
                17 => "mal",
                20 => "icosi",
                36 => "feta",
                100 => "hecto",
                _ => unreachable!(),
            }),
            _ => None,
        }
    }

    fn get_name_particles(&mut self, x: u32, prefix: bool) -> Vec<(u32, bool)> {
        if let Some(result) = if prefix {
            self.particle_cache[x as usize].0.as_ref()
        } else {
            self.particle_cache[x as usize].1.as_ref()
        } {
            return result.to_vec();
        }

        let result = match x {
            // If we have an exact match use it
            0..=13 | 16 | 17 | 20 | 36 | 100 => vec![(x, prefix)],
            // If we have a prime then,
            // If we are not in a multiplicative prefix we use "un"
            // otherwise we return "hen" + "sna"
            _ if self.sieve.is_prime(x as usize) => {
                if !prefix {
                    [vec![(1, true)], self.get_name_particles(x - 1, false)].concat()
                } else {
                    [
                        vec![(0, true)],
                        self.get_name_particles(x - 1, true),
                        vec![(u32::MAX, true)],
                    ]
                    .concat()
                }
            }
            _ => {
                let sqrt_x = (x as f32).sqrt();

                struct FactorPair {
                    prefix: Vec<(u32, bool)>,
                    suffix: Vec<(u32, bool)>,
                    pair: (u32, u32),
                }

                impl FactorPair {
                    fn get_list(self) -> Vec<(u32, bool)> {
                        [self.prefix, self.suffix].concat()
                    }
                }

                // -sna- does not count if it's new
                // but only if it wouldn't be there if this is not a prefix
                let num_parts = |x: &FactorPair| {
                    x.prefix.len() + x.suffix.len()
                        - x.prefix
                            .iter()
                            .rev()
                            .take_while(|x| x.0 == u32::MAX)
                            .collect::<Vec<_>>()
                            .len()
                };

                if let Some(cached_factor) = self.used_factor_cache[x as usize] {
                    FactorPair {
                        prefix: self.get_name_particles(cached_factor, true),
                        suffix: self.get_name_particles(x / cached_factor, prefix),
                        pair: (cached_factor, x / cached_factor),
                    }
                    .get_list()
                } else {
                    // prime_powers is a list of all of the prime powers of the factors of a number
                    // 180 = 2*2*3*3*5, so prime_factors contains [(2,2), (3,2), (5,1)]
                    // So prime_powers will contain [[1,2,4], [1,3,9], [1,5]]
                    let parts = self
                        .sieve
                        .factor(x as usize)
                        .unwrap()
                        .into_iter()
                        .map(|(factor, count)| {
                            (0..=count)
                                .map(|exp| factor.pow(exp as u32))
                                .collect::<Vec<_>>()
                        }) // this is "prime_powers"
                        .multi_cartesian_product()
                        .map(|x| x.into_iter().fold(1_u32, |acc, x| acc * x as u32))
                        .sorted()
                        //.unique() // Not needed, no duplicates will be generated
                        .filter(|&x| (x as f32 <= sqrt_x) && (x != 1)) // this is "factors"
                        .map(|factor: u32| FactorPair {
                            prefix: self.get_name_particles(factor, true),
                            suffix: self.get_name_particles(x / factor, prefix),
                            pair: (factor, x / factor),
                        })
                        .collect::<Vec<_>>();

                    let min_parts = parts.iter().map(num_parts).min().unwrap();

                    let factor_pair_result = parts
                        .into_iter()
                        .filter(|x| num_parts(x) == min_parts)
                        .min_by_key(|x| (x.pair.0 as isize - x.pair.1 as isize).abs() as u32)
                        .unwrap();

                    self.used_factor_cache[x as usize] = Some(factor_pair_result.pair.0);

                    factor_pair_result.get_list()
                }
            }
        };

        if prefix {
            self.particle_cache[x as usize].0 = Some(result.clone());
        } else {
            self.particle_cache[x as usize].1 = Some(result.clone());
        }

        result
    }

    fn slugs_from_particles(&self, particles: &Vec<(u32, bool)>) -> Vec<&'static str> {
        particles
            .iter()
            .map(|x| {
                if x.1 {
                    self.get_root_prefix(x.0).unwrap()
                } else {
                    self.get_root_base(x.0).unwrap()
                }
            })
            .collect()
    }

    fn name_from_particles(&self, particles: Vec<(u32, bool)>) -> String {
        if particles.len() == 1 {
            return {
                match particles[0].0 {
                    // Need these two because the root base is modified
                    10 => String::from("decimal"),
                    13 => String::from("baker's dozenal"),
                    _ => String::from(self.get_root_base(particles[0].0).unwrap()),
                }
            };
        }

        let particle_strs = self.slugs_from_particles(&particles);

        // 1 is delete the last character in the first, 2 is the delete the first character in the
        //   second
        let vowel_delete_instructions = [
            vec![0],
            particles
                .windows(2)
                .map(|window| {
                    let (current, next) = (window[0], window[1]);
                    match current.0 {
                        // roots ending in an i
                        2 | 3 | 20 => match next.0 {
                            // -ii- => i
                            // -iu- => i
                            1 => 2,
                            // 20 only has a vowel if it's in prefix form
                            20 => {
                                if next.1 {
                                    1
                                } else {
                                    0
                                }
                            }
                            _ => 0,
                        },
                        // roots ending in an o or an a
                        4..=12 | 36 | 100 | u32::MAX => match next.0 {
                            // in all cases, o and a are omitted in favor of the other vowel
                            1 | 8 => 1,
                            // these forms only have vowels if they are prefixes
                            9 | 20 => {
                                if next.1 {
                                    1
                                } else {
                                    0
                                }
                            }
                            // these forms only have vowels if they are base forms
                            11 => {
                                if next.1 {
                                    0
                                } else {
                                    1
                                }
                            }
                            _ => 0,
                        },
                        _ => 0,
                    }
                })
                .collect::<Vec<_>>(),
            vec![0],
        ]
        .concat();

        // Now we combine.  Example:
        // 17482 = binhentrihexasnicosinbielevenary
        // our two lists will be:
        // [   "bi", "un", "hen", "tri", "hexa", "sna", "icosi", "un", "bi", "elevenary"]
        // [0, 2,    0,    0,     0,     0,      1,     2,       0,    0,    0]
        // At each iteration we look at the the current and previous of the command list to decide
        // if we will remove the first or the last character of this particle string.
        // Due to how this system is structured, we will never need to do both.
        let mut result = String::from("");

        for (particle_str, command) in particle_strs
            .into_iter()
            .zip(vowel_delete_instructions.windows(2))
        {
            if command[0] == 2 {
                // if our leading vowel was deleted
                result.push_str(&particle_str[1..]);
            } else if command[1] == 1 {
                result.push_str(&particle_str[..particle_str.len() - 1]);
            } else {
                result.push_str(particle_str);
            }
        }

        result
    }

    fn get_name(&mut self, x: u32) -> String {
        let particles = self.get_name_particles(x, false);
        self.name_from_particles(particles)
    }

    fn get_abbr(&mut self, x: u32) -> String {
        if let Some(result) = self.abbr_map.get_by_left(&x) {
            return decode_string(result);
        }
        if x == 16 {
            self.abbr_map
                .insert(16, encode_string(&String::from("HEX")));
            return String::from("HEX");
        }

        // uppercase and filter out non-letters
        let name = self
            .get_name(x)
            .to_ascii_uppercase()
            .chars()
            .filter(|c| c.is_alphabetic())
            .collect::<String>();

        // remove vowels after 3rd letter
        let name = format!(
            "{}{}",
            name[..3].to_owned(),
            name[3..name.len() - 1]
                .chars()
                .filter(|c| match c {
                    'A' | 'E' | 'I' | 'O' | 'U' => false,
                    _ => true,
                })
                .collect::<String>()
        );

        // initialize a new prefix cache
        let mut prefix_cache =
            IndexSet::<u64, RandomState>::with_capacity_and_hasher(1_000, RandomState::new());

        for i in 2.. {
            // TODO: find a way to effectively reuse this cache between sizes
            prefix_cache.clear();
            prefix_cache.insert(encode_string(&name[0..1].to_owned()));

            for letter in name.chars().skip(1) {
                let mut iter_cache = IndexSet::<u64, RandomState>::with_hasher(RandomState::new());
                for prefix in prefix_cache.iter().map(decode_string) {
                    if prefix.len() == i + 1 {
                        continue;
                    }

                    let new_prefix = format!("{}{}", prefix, letter);
                    let new_encoded_prefix = encode_string(&new_prefix);
                    if !prefix_cache.contains(&new_encoded_prefix)
                        && iter_cache.insert(new_encoded_prefix)
                        && new_prefix.len() == i + 1
                        && !self.abbr_map.contains_right(&new_encoded_prefix)
                    {
                        self.abbr_map.insert(x, new_encoded_prefix);
                        return new_prefix;
                    }
                }
                // add the new prefixes to the cache in order
                prefix_cache.extend(iter_cache.into_iter());
            }
        }

        unreachable!();
    }
}

fn dec_to_sex(mut x: u32) -> String {
    if x == 0 {
        return String::from("0");
    }

    let mut digits: Vec<u32> = vec![];
    while x > 0 {
        digits.push(x % 6);
        x /= 6;
    }
    digits
        .into_iter()
        .rev()
        .map(|x| from_digit(x as u32, 6).unwrap())
        .collect::<String>()
}

fn print_line(gen: &mut MisalianNameGenerator, x: u32) {
    let particles = gen.get_name_particles(x, false);
    let name = gen.get_name(x);
    let abbr = gen.get_abbr(x);
    if particles.len() == 1 {
        println!("BASE-{} (DEC{}): {} ({})", dec_to_sex(x), x, name, abbr);
    } else {
        println!(
            "BASE-{} (DEC{}): {} ({}) ({})",
            dec_to_sex(x),
            x,
            name,
            gen.slugs_from_particles(&particles).join("-"),
            abbr
        );
    }
}

fn main() {
    let top = 36_usize.pow(4 as u32);
    //let top = 20_000;
    let mut gen = MisalianNameGenerator::new(top);

    for i in 0..=top {
        print_line(&mut gen, i as u32);
    }
}
